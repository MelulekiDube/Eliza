/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eliza;

import java.util.Scanner;

/**
 *
 * @author dube_
 */
public class Eliza {

    /**
     * @param args the command line arguments
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception {
	Database db = new Database();
	Scanner sc = new Scanner(System.in);
	System.out.println("Start Typing");
	while(sc.hasNextLine()){
	    System.out.println(db.getResponse(sc.nextLine()));
	}
    }
    
}
