/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eliza;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 *
 * @author dube_
 */
public class Database {

    public static final String DATABASE_LOCATION = "./resources/Eliza.dat"; // this is the file that will keep the responses to certain words.
    public static final String TRANSPOSE_FILE = "./resources/transpose.dat";
    private final String KEY_INDICATOR = "@KWD@";
    private final File infor_file;
    private final Map<String, List<String>> kwd_response_map;

    public Database() throws Exception {
	infor_file = new File(DATABASE_LOCATION);
	kwd_response_map = new HashMap<>();
	parseFile();
    }

    public final void parseFile() throws Exception {
	try (BufferedReader br = new BufferedReader(new FileReader(infor_file))) {
	    String line = br.readLine();
	    List<String> temp_answers = null;
	    String key = "";
	    while (line != null) {
		if (line.equals(KEY_INDICATOR)) {
		    if(temp_answers != null) insertKeysAndValue(key, temp_answers); // insert the finished response and key to the map
		    temp_answers = new ArrayList<>(); // create a new list of responses to the keyWord
		    key = br.readLine(); // read in the key word
		}
		else temp_answers.add(line);
		line = br.readLine();
	    }
	    insertKeysAndValue(key, temp_answers);// since we finshed reading file without inserting these ones we want to insert them now that we done reading the file
	}
    }
    
    public String getResponse(String question){
	Random rd = new Random();
	List<String> list= kwd_response_map.get(question.toLowerCase());
	int respNumber = rd.nextInt(list.size());
	return list.get(respNumber);
    }

    private void insertKeysAndValue(String key, List<String> temp_answers) {
	kwd_response_map.put(key.toLowerCase(), temp_answers);
    }
}
