# Eliza implemented in java

This is a java implementation of ELIZA the chatbot. 

As of yet the implementation is not complete, Eliza is just able to respond to a simple hello.


## To run the the bot:
- Type ```make``` on the command line to compile the java classes (these asre compiled to the bin/eliza directory)
- Type ```make run``` to run Eliza, then you will be prompted to start typing. For now only words listed as key words in the resource/eliza.dat file can be reponded to.